---
title: Mushroom analysis
subtitle: Project for M9211 Bayesian methods
authors:
  - name: Štěpán Zapadlo
    affiliation: Masaryk University

format:
  html:
    comments:
      hypothesis: false
    theme:
      light: cerulean
      dark: darkly
    toc: true
    html-math-method: mathjax
    df-print: kable
    standalone: true
    embed-resources: true
    lightbox: true
    code-tools: true
  pdf:
    documentclass: scrartcl
    keep-tex: true

    mainfont: "TeX Gyre Pagella"
    #mathfont: "TeX Gyre Pagella Math"
    monofont: "Latin Modern Mono"
    geometry:
      - top=30mm
      - left=20mm
      - heightrounded

    include-in-header:
      - ./mathematics/pdf.tex
      - ./mathematics/general.tex
      - ./mathematics/linear-algebra.tex
      - ./mathematics/statistics.tex
      - ./mathematics/special.tex
      - ./mathematics/overrides.tex
    
    toc: true

    fig-align: center
    fig-pos: H
    fig-asp: 0.5
    
    fig-format: png
    mermaid-format: png
    df-print: tibble

execute:
  freeze: auto
  warning: false

bibliography: bibliography.bib
---

:::{style="display: none"}
{{< include mathematics.qmd >}}
:::

```{R}
#| code-fold: true
#| echo: !expr knitr::is_html_output()
#| output: false
library("HDInterval")
library("invgamma")
library("tibble")
library("tidyr")
library("dplyr")
library("ggplot2")
library("GGally")
library("purrr")

theme_set(theme_bw()) # theme_gray()
```

## Introduction

In this project, we will focus on *mushroom classification* dataset, which can be found [here](https://www.kaggle.com/datasets/vishalpnaik/mushroom-classification-edible-or-poisonous). It is a plain `.csv` file, so loading is trivial.

```{R}
#| code-fold: true
data.base <- read.csv("data/mushroom.csv")
data.base$ID <- factor(seq(1, nrow(data.base)))
```

:::{.content-visible unless-format="pdf"}
```{R}
#| echo: false
head(data.base)
```
:::

There are a number of categorical columns, among which we shall be mostly interested in *class*, which indicates whether the mushroom is edible or not. Moreover, biologically speaking, we expect the continuous variables, namely *cap diameter*, *stem height* and *stem width*, to be positive real numbers. This condition will also become very important soon, as we will discuss and use the transformation of our data.

```{R}
#| code-fold: true
data <- data.base |> 
  na.omit() |>
  filter(cap.diameter > 0 & stem.height > 0 & stem.width > 0)
```

### Normality analysis

By plotting the histograms and QQ plots of the continuous variables, see @fig-hist-orig, it becomes apparent that no normality assumptions can be made.

```{R}
#| code-fold: true
#| echo: !expr knitr::is_html_output()
#| layout-ncol: 3
#| layout-nrow: 2
#| label: fig-hist-orig
#| fig-cap: "Histograms and QQ plots of continuous variables"
#| fig-subcap: 
#|   - "histogram of cap diameter"
#|   - "histogram of stem height"
#|   - "histogram of stem width"
#|   - "QQ plot of cap diameter"
#|   - "QQ plot of stem height"
#|   - "QQ plot of stem width"

data |> ggplot(aes(
        x = cap.diameter, 
        y = after_stat(density), 
        fill = class)) + 
    geom_histogram(
        color = "black", 
        linewidth = 0.2, 
        bins = 1 + ceiling(log2(nrow(data)))) + 
    facet_grid(class ~ .) 
data |> ggplot(aes(
        x = stem.height, 
        y = after_stat(density), 
        fill = class)) + 
    geom_histogram(
        color = "black", 
        linewidth = 0.2, 
        bins = 1 + ceiling(log2(nrow(data)))) + 
    facet_grid(class ~ .) 
data |> ggplot(aes(
        x = stem.width, 
        y = after_stat(density), 
        fill = class)) + 
    geom_histogram(
        color = "black", 
        linewidth = 0.2, 
        bins = 1 + ceiling(log2(nrow(data)))) + 
    facet_grid(class ~ .) 

qqnorm(data$cap.diameter, 
    pch = 1, 
    frame = FALSE)
qqline(data$cap.diameter, 
    col = "steelblue", 
    lwd = 2)

qqnorm(data$stem.height, 
    pch = 1, 
    frame = FALSE)
qqline(data$stem.height, 
    col = "steelblue", 
    lwd = 2)

qqnorm(data$stem.width, 
    pch = 1, 
    frame = FALSE)
qqline(data$stem.width, 
    col = "steelblue", 
    lwd = 2)
```

Nonetheless, the logarithmic transformation of these columns grants us at least approximate normality, see @fig-hist-log.

```{R}
#| code-fold: show
data <- data |> mutate(
    log.cap.diameter = log(cap.diameter),
    log.stem.height = log(stem.height),
    log.stem.width = log(stem.width)
)
```

```{R}
#| code-fold: true
#| echo: !expr knitr::is_html_output()
#| layout-ncol: 3
#| layout-nrow: 2
#| label: fig-hist-log
#| fig-cap: "Histograms of logs of continuous variables"
#| fig-subcap: 
#|   - "histogram of log of cap diameter"
#|   - "histogram of log of stem height"
#|   - "histogram of log of stem width"
#|   - "QQ plot of log of cap diameter"
#|   - "QQ plot of log of stem height"
#|   - "QQ plot of log of stem width"


data |> ggplot(aes(
        x = log.cap.diameter, 
        y = after_stat(density), 
        fill = class)) + 
    geom_histogram(
        color = "black", 
        linewidth = 0.2, 
        bins = 1 + ceiling(log2(nrow(data)))) + 
    facet_grid(class ~ .) 
data |> ggplot(aes(
        x = log.stem.height, 
        y = after_stat(density), 
        fill = class)) + 
    geom_histogram(
        color = "black", 
        linewidth = 0.2, 
        bins = 1 + ceiling(log2(nrow(data)))) + 
    facet_grid(class ~ .) 
data |> ggplot(aes(
        x = log.stem.width, 
        y = after_stat(density), 
        fill = class)) + 
    geom_histogram(
        color = "black", 
        linewidth = 0.2, 
        bins = 1 + ceiling(log2(nrow(data)))) + 
    facet_grid(class ~ .) 

qqnorm(data$log.cap.diameter, 
    pch = 1, 
    frame = FALSE)
qqline(data$log.cap.diameter, 
    col = "steelblue", 
    lwd = 2)

qqnorm(data$log.stem.height, 
    pch = 1, 
    frame = FALSE)
qqline(data$log.stem.height, 
    col = "steelblue", 
    lwd = 2)

qqnorm(data$log.stem.width, 
    pch = 1, 
    frame = FALSE)
qqline(data$log.stem.width, 
    col = "steelblue", 
    lwd = 2)
```

To better understand the relationship between the continuous variables and how the log transformation affects them, see @fig-pairs. Also, notice that the (density of) *cap diameter* is **bimodal**. The second cluster with higher cap diameters (with an average approx. 10 times higher than the mean of others) also corresponds to the same color of the mushrooms' cap -- as such, it may indicate an error in data, where the researchers used *millimeters* instead of *centimeters* for the cap diameter of this mushroom cluster.

```{R}
#| code-fold: true
#| echo: !expr knitr::is_html_output()
#| layout-nrow: 2
#| label: fig-pairs
#| fig-cap: "Pair plots of continuous variables per class"
#| fig-subcap: 
#|   - "pair plot of continuous variables"
#|   - "pair plot of logs of continuous variables"
data.cont <- data |> select(class,
    cap.diameter, stem.height, stem.width,
    log.cap.diameter, log.stem.height, log.stem.width)

ggpairs(data.cont, columns = 2:4, ggplot2::aes(colour=class)) 
ggpairs(data.cont, columns = 5:7, ggplot2::aes(colour=class)) 
```

### Summary statistics

Let us now calculate statistics from our experiment data.

```{R}
#| code-fold: show

data.poison <- data |> filter(class == 'p')
data.poison.cont <- data.poison |> 
    select (log.cap.diameter, log.stem.height, log.stem.width)

data.edible <- data |> filter(class == 'e')
data.edible.cont <- data.edible |> 
    select (log.cap.diameter, log.stem.height, log.stem.width)

poison.stat <- rbind(
    mean = data.poison.cont |> summarise_all(mean),
    var = data.poison.cont |> summarise_all(var),
    n = data.poison.cont |> summarise_all(length)
)

edible.stat <- rbind(
    mean = data.edible.cont |> summarise_all(mean),
    var = data.edible.cont |> summarise_all(var),
    n = data.edible.cont |> summarise_all(length)
)
```

We can see, among other statistics, the mean height of mushrooms (from this dataset) is `{r} mean(data$stem.height)` cm with quite a high standard deviation of `{r} sd(data$stem.height)` cm. In the @tbl-stats-class, statistics of the log-transformed columns are available.

```{R}
#| echo: !expr knitr::is_html_output()
#| layout-nrow: 2
#| label: tbl-stats-class
#| tbl-cap: "Summary statistics per class"
#| tbl-subcap:
#|   - Poisonous mushrooms
#|   - Edible mushrooms

poison.stat
edible.stat
```

## Computations

### Jeffreys prior

As I possess little to no knowledge about mushrooms and their properties, we shall use *Jeffreys prior* for the semi-conjugated prior distribution. In our case it means
$$
p(\mu, \sigma^2) = \jeffreys(\mu, \sigma^2) \propto \frac 1 {\sigma^3}.
$$

```{R}
#| echo: !expr knitr::is_html_output()
jeffrey.dist <- function (variance) {
  return(1/(variance^3))
}
```

#### Jeffreys prior as a limit process

:::{.callout-warning}
Note that this is truly an improper distribution and as such, any attempts to sample from it will fail *spectacularly*. To be even more clear, not even using a "cut" distribution like
$$
\jeffreys_{\ve}(\mu, \sigma^2) = \int_\ve^{\sigma^2} \frac 1 {s^3} \, \d s.
$$
would help us simulate such probability distribution.
:::

Recall that the semi-conjugated distributions for unknown $(\mu, \sigma^2)$ are
$$
\rnd \sigma^2 \sim \invgammaDist\brackets{\frac {b_0} 2, \frac {b_0} 2 \sigma_0^2} \quad \& \quad \brackets{\rnd \mu \condp \rnd \sigma^2 = \sigma^2} \sim \normalD{\mu_0, \frac {\sigma^2} {a_0}},
$$
where $a_0, b_0, \mu_0, \sigma_0^2$ are hyper-parameters. The Jeffreys prior thus corresponds to limit processes (see [this lecture](https://www.jarad.me/courses/stat544/slides/Ch02/Ch02b.pdf)) for variance
$$
\alpha \to 0^+ \quad \land \quad \beta \to 0^+
$$
and similarly for mean
$$
\mu \to 0 \quad \land \quad \sigma^2 \to \infty
$$
in their respective distributions, that is $\invgammaDist(\alpha, \beta)$ and $\normalD{\mu, \sigma^2}$. Returning to our hyper-parameter choice, it corresponds to
\begin{align*}
b_0 &= 0, & \sigma_0^2 &= 0, \\
\mu_0 &= 0, & a_0 &= 0.
\end{align*} 

::: {.callout-caution}
Bear in mind that these values are only true in the *limit sense* and as such cannot be used for sampling directly.
:::

#### Simulation of posteriors

Let us also mainly focus on the **stem height** variable, as it seems to be the closest to the *normal distribution* (see @fig-pairs). Hence we can now calculate the posterior parameters -- the posterior probability will be an inverse gamma distribution, as Jeffreys prior is also inverse gamma in the limit sense (see above).

```{R}
#| code-fold: show
#| echo: !expr knitr::is_html_output()
# (in the limit sense) parameters of the prior distribution
prior <- tibble(alpha = 0, beta = 0, mu = 0, 
	sigma2 = 0, b = 0, a = 0)

# parameters of the posteriors
posterior.poison <- with(prior, tibble(
    y.bar = poison.stat$log.stem.height[1],
    s2 = poison.stat$log.stem.height[2],
    n = poison.stat$log.stem.height[3],
    mu = (n * y.bar + a * mu) / (n + a),
    sigma2 = ((n - 1) * s2 + b * sigma2 + 
        a * n * (y.bar - mu)^2 / (n + a)) / (n + b), 
    a = n + a, 
    b = n + b, 
    alpha = b / 2, 
    beta = b * sigma2 / 2
  ))

posterior.edible <- with(prior, tibble(
    y.bar = edible.stat$log.stem.height[1],
    s2 = edible.stat$log.stem.height[2],
    n = edible.stat$log.stem.height[3],
    mu = (n * y.bar + a * mu) / (n + a),
    sigma2 = ((n - 1) * s2 + b * sigma2 + 
        a * n * (y.bar - mu)^2 / (n + a)) / (n + b), 
    a = n + a, 
    b = n + b, 
    alpha = b / 2, 
    beta = b * sigma2 / 2
  ))
```

With the posterior parameters calculated, we can simulate the simultaneous distribution for $(\rnd \mu, \rnd \sigma^2)$.

```{R}
#| code-fold: show
#| echo: !expr knitr::is_html_output()
N <- 1e6
bins <- 100

sim.posterior.poison <- tibble(
    sigma2 = rinvgamma(N, 
        posterior.poison$alpha, 
        posterior.poison$beta), 
    mu = rnorm(sigma2, 
        posterior.poison$mu, 
        sqrt(sigma2 / posterior.poison$a)), 
    prec = 1/sigma2,
    model = "posterior.poison"
  )
sim.posterior.edible <- tibble(
    sigma2 = rinvgamma(N, 
        posterior.edible$alpha, 
        posterior.edible$beta), 
    mu = rnorm(sigma2, 
        posterior.edible$mu, 
        sqrt(sigma2 / posterior.edible$a)), 
    prec = 1/sigma2,
    model = "posterior.edible"
  )

sim <- rbind(sim.posterior.poison, sim.posterior.edible)
```

Having computed the posterior parameters and also simulated their respective distributions, we can plot the results, see @fig-jeffrey-sims-result.

```{R}
#| code-fold: true
#| echo: !expr knitr::is_html_output()
#| layout-ncol: 2
#| layout-nrow: 2
#| label: fig-jeffrey-sims-result
#| fig-cap: "Simulation of posterior probability"
#| fig-subcap: 
#|   - "simulation of the expected value"
#|   - "excepted value kernel estimate"
#|   - "simulation of the variance"
#|   - "variance kernel estimate"

sim |> ggplot(aes(x = mu, color = model, 
        fill = model, y = after_stat(density))) + 
    geom_histogram(bins = bins, color = "black", size = 0.5) + 
    labs(x = "expected value of stem height", y = "density") + 
    xlim(c(1.74, 1.88)) +
    facet_wrap("model", nrow = 3, scales = "free_y")
sim |> ggplot(aes(x = mu, color = model, fill = model)) + 
    geom_density(linewidth = 0.7, alpha = 0.1) + 
    labs(x = "expected value of stem height", y = "density") + 
    geom_area(stat = "function", fun = \(s) 100, 
        aes(color = "prior", fill = "prior"), alpha = 0.15) +
    xlim(c(1.74, 1.88))

sim |> ggplot(aes(x = sigma2, color = model, 
        fill = model, y = after_stat(density))) + 
    geom_histogram(bins = bins, color = "black", size = 0.5) + 
    labs(x = "variance of stem height", y = "density") + 
    facet_wrap("model", nrow = 3, scales = "free_y")
sim |> ggplot(aes(x = sigma2, color = model, fill = model)) + 
    geom_density(linewidth = 0.7, alpha = 0.1) + 
    labs(x = "variance of stem height", y = "density") + 
    geom_area(stat = "function", fun = \(s) 1/(s^3), 
        aes(color = "prior", fill = "prior"), alpha = 0.15) +
    xlim(c(0.15, 0.21)) + ylim(c(0, 300))
```

To get an even better picture of the distribution at hand, one can plot the simulation onto the 2D plane and draw the contour lines of the corresponding simultaneous 2D multivariate *inverse gamma-normal* distribution, see @fig-jeffrey-sims-result-2D.

```{R}
#| label: fig-jeffrey-sims-result-2D
#| echo: !expr knitr::is_html_output()
#| fig-cap: Simultaneous 2D normal distribution contour from simulation
#| code-fold: true
sim |> ggplot(aes(x = mu, y = sigma2, group = model, color = model)) + 
  geom_point(size = 0.5, alpha = 0.5) +
  geom_density_2d(color = "black", linewidth = 0.5, bins = 15) + 
  geom_contour(stat = "function", fun = \(x,y) 1/(y^3), aes(color = "prior", fill = "prior")) +
  labs(x = "expected value of stem height", y = "variance of stem height") + 
  facet_wrap("model", ncol = 3, scales = "free")
```

#### Posterior statistics of modeled parameters

Given our simulated distributions for $\rnd \mu$ and $\rnd \sigma^2$, we can estimate the expected values, *maximum aposteriori probability* estimates, etc, see @tbl-jeffreys-posterior-stats.

```{R}
#| code-fold: true
#| echo: !expr knitr::is_html_output()
density.max <- function(x, ...) {
  pdf.kde <- density(x, ...)
  index <- which.max(pdf.kde$y)
  return(pdf.kde$x[index])
}

implode <- function(..., sep='') {
     paste(..., collapse=sep)
}
```

```{R}
#| code-fold: show
#| label: tbl-jeffreys-posterior-stats
#| tbl-cap: Statistics of posterior distributions for $\rnd \mu$ and $\rnd \sigma^2$ using Jeffreys prior
sim |> 
  group_by(model) |> 
  summarise(
    mu.MAP = density.max(mu, 
		  from = 1.5, to = 2.0, n = 1024), 
    mu.mean = mean(mu), 
    mu.sd = sd(mu), 
    mu.ETI.l = t(quantile(mu, c(0.025))),
    mu.ETI.h = t(quantile(mu, c(0.975))),
    mu.HDI.l = t(hdi(mu, credMass = 0.95))[1], 
    mu.HDI.h = t(hdi(mu, credMass = 0.95))[2], 
    
    sigma2.MAP = density.max(sigma2, from = 0.1, 
		  to = 0.25, n = 1024), 
    sigma2.mean = mean(sigma2), 
    sigma2.sd = sd(sigma2), 
    sigma2.ETI.l = t(quantile(sigma2, c(0.025))),
    sigma2.ETI.h = t(quantile(sigma2, c(0.975))),
    sigma2.HDI.l = t(hdi(sigma2, credMass = 0.95))[1], 
    sigma2.HDI.h = t(hdi(sigma2, credMass = 0.95))[2],  
  )
```

For example, we can see that the maximum aposteriori probability $\mu$ value is $6.42$ cm (after $\exp{}$ transform) for edible mushrooms while $5.80$ cm for the poisonous kind. What's more, edible mushrooms also have lower expected posterior variance.

#### Predictive distributions

Last but not least, one can calculate the predictive posterior distribution. In our case, as we worked with an improper prior, it is unfortunately impossible to simulate a marginal predictive distribution. Notice the little-to-no distinguishability between edible and poisonous mushrooms in the posterior predictive distribution, see @fig-jeffrey-pred -- remember that posterior distribution for both $\rnd \mu$ and $\rnd \sigma^2$ showed promising signs of the ability to distinguish edible and poisonous mushrooms based on their stem height.

```{R}
#| code-fold: show
sim.pred <- sim |> 
  mutate(
    z = rnorm(mu, mu, sqrt(sigma2)), 
    model = ifelse(model == "prior", "marginal", model)
  )
```

```{R}
#| code-fold: true
#| echo: !expr knitr::is_html_output()
#| layout-ncol: 2
#| label: fig-jeffrey-pred
#| fig-cap: "Simulation of the predictive posterior distribution for the stem height"
#| fig-subcap: 
#|   - "simulation of the predictive distribution"
#|   - "kernel estimate of the predictive distribution"

sim.pred |> ggplot(aes(x = z, color = model, 
      fill = model, y = after_stat(density))) + 
    geom_histogram(bins = bins, color = "black", size = 0.5) + 
    labs(x = "stem height", y = "density") + 
    facet_wrap("model", nrow = 3, scales = "free_y")

sim.pred |> ggplot(aes(x = z, color = model, fill = model)) + 
    geom_density(linewidth = 0.7, alpha = 0.1) + 
    labs(x = "stem height", y = "density")
```

#### Student $t$-distribution

It should be noted, that @hoff2009first mentions that for a choice of non-informative prior $p(\tht, \sigma^2) = \frac 1 {\sigma^2}$, which leads to the same hyper-parameter choice as with *our* Jeffreys prior, and the *biased sample variance* $\frac{n - 1} n s^2$ as an estimate of the variance, then the posterior distribution of $\rnd \theta$ will be the Students $t$-distribution, more precisely
$$
\brackets{\frac {\rnd \tht - \avg y} {s / \sqrt{n}} \condp y_1, \dots, y_n} \sim t_{n - 1}.
$$

In our case, we have roughly 60k rows in the dataset, so the Student's $t$-distribution will be neigh indistinguishable from the normal distribution, as can be seen in @fig-jeffrey-student-t.

```{R}
#| code-fold: true
#| echo: !expr knitr::is_html_output()
#| layout-ncol: 2
#| label: fig-jeffrey-student-t
#| fig-cap: "Comparison of posterior $\\rnd \\tht$ as a normal distribution and Student's $t$-distribution"
#| fig-subcap: 
#|   - "simulations"
#|   - "kernel estimates"
# Calculating biased sample variance
poison.stat.t <- poison.stat |>
  as.data.frame() |>
  t() |> as.data.frame() |>
  mutate(
  	bias.var = (n - 1) / n * var
  ) |>
  t() |> as.data.frame()

edible.stat.t <- edible.stat |>
  as.data.frame() |>
  t() |> as.data.frame() |>
  mutate(
  	bias.var = (n - 1) / n * var
  ) |>
  t() |> as.data.frame()

# Recalculating posterior parameters
posterior.poison.t <- with(prior, tibble(
    y.bar = poison.stat.t$log.stem.height[1],
    s2 = poison.stat.t$log.stem.height[4],
    n = poison.stat.t$log.stem.height[3],
    mu = (n * y.bar + a * mu) / (n + a),
    sigma2 = ((n - 1) * s2 + b * sigma2 + 
        a * n * (y.bar - mu)^2 / (n + a)) / (n + b), 
    a = n + a, 
    b = n + b, 
    alpha = b / 2, 
    beta = b * sigma2 / 2
  ))

posterior.edible.t <- with(prior, tibble(
    y.bar = edible.stat.t$log.stem.height[1],
    s2 = edible.stat.t$log.stem.height[4],
    n = edible.stat.t$log.stem.height[3],
    mu = (n * y.bar + a * mu) / (n + a),
    sigma2 = ((n - 1) * s2 + b * sigma2 + 
        a * n * (y.bar - mu)^2 / (n + a)) / (n + b), 
    a = n + a, 
    b = n + b, 
    alpha = b / 2, 
    beta = b * sigma2 / 2
  ))

sim.posterior.poison.t <- tibble(
    sigma2 = rinvgamma(N, 
        posterior.poison.t$alpha, 
        posterior.poison.t$beta), 
    mu = rnorm(sigma2, 
        posterior.poison.t$mu, 
        sqrt(sigma2 / posterior.poison.t$a)), 
    prec = 1/sigma2,
    model = "posterior.poison.t"
  )
sim.posterior.edible.t <- tibble(
    sigma2 = rinvgamma(N, 
        posterior.edible.t$alpha, 
        posterior.edible.t$beta), 
    mu = rnorm(sigma2, 
        posterior.edible.t$mu, 
        sqrt(sigma2 / posterior.edible.t$a)), 
    prec = 1/sigma2,
    model = "posterior.edible.t"
  )

sim.t <- rbind(sim.posterior.poison.t, sim.posterior.edible.t)

sim.all <- rbind(sim, sim.t)

sim.all |> ggplot(aes(x = mu, color = model, 
        fill = model, y = after_stat(density))) + 
    geom_histogram(bins = bins, color = "black", size = 0.5) + 
    labs(x = "expected value of stem height", y = "density") + 
    xlim(c(1.74, 1.88)) +
    facet_wrap("model", nrow = 3, scales = "free")
sim.all |> ggplot(aes(x = mu, color = model, fill = model)) + 
    geom_density(linewidth = 0.7, alpha = 0.1) + 
    labs(x = "expected value of stem height", y = "density") + 
    xlim(c(1.74, 1.88))
```

### More informative prior

Our choice of Jeffreys prior for its low informativeness made simulations certainly harder to perform. Thus, we should also consider a case of a more informative prior about the distribution of the parameters. 

Since my knowledge has not improved in those few paragraphs, I tried searching for the average mushroom height on Google, but I returned empty-handed. The next best thing that came into my mind was to employ ChatGPT 3.5 to get the prior. Thus I used the following prompt:

> Suppose that the mushroom stem height is governed by the normal distribution. What should be the expected value and variance?

ChatGPT answered with the following response (abbreviated):

> (...)
> Suppose the mean height of mushroom stems is 2 inches, and the standard deviation (a measure of spread) is 0.5 inches. In this case:
>
> 1. Expected value (mean) = 2 inches
> 2. Variance = (Standard deviation)^2 = (0.5 inch)^2 = 0.25 square inch
>
> (...)

Hence we will from now on assume $\Expect{\rnd \mu} = \log(5.08 \, \mathrm{cm}) = 1.63$ and $\Variance{\rnd \sigma^2} = \log^2\brackets{\frac {2.54} 4 \, \mathrm{cm}^2} = 0.2062$. Recall that the expected value of inverse gamma distribution $\invgammaDist(\alpha, \beta)$ is given by $\frac {\beta} {\alpha - 1}$ for $\alpha > 1$ (for lower values of $\alpha$, the expected value of the distribution does not exist), which can again be converted into our hyper-parameters to yield
$$
\frac {\frac {b_0} 2 \sigma_0^2} {\frac {b_0} 2 - 1} = \frac {\sigma_0^2} {1 - \frac 2 {b_0}} \Bigg|_{b_0 = 100, \sigma_0^2 = 0.2021} = 0.2062.
$$
Similarly, we achieve the desired expected value of $\mu$ by setting $\mu_0 = 1.63$ and $a_0 = 500$.

```{R}
#| code-fold: show
#| echo: !expr knitr::is_html_output()
prior.gpt <- tibble(b = 100, sigma2 = 0.2021, 
  mu = 1.63, a = 500, alpha = b / 2, beta = b * sigma2 / 2)
```

```{R}
#| code-fold: true
#| echo: !expr knitr::is_html_output()
posterior.poison.gpt <- with(prior.gpt, tibble(
    y.bar = poison.stat$log.stem.height[1],
    s2 = poison.stat$log.stem.height[2],
    n = poison.stat$log.stem.height[3],
    mu = (n * y.bar + a * mu) / (n + a),
    sigma2 = ((n - 1) * s2 + b * sigma2 + 
        a * n * (y.bar - mu)^2 / (n + a)) / (n + b), 
    a = n + a, 
    b = n + b, 
    alpha = b / 2, 
    beta = b * sigma2 / 2
  ))

posterior.edible.gpt <- with(prior.gpt, tibble(
    y.bar = edible.stat$log.stem.height[1],
    s2 = edible.stat$log.stem.height[2],
    n = edible.stat$log.stem.height[3],
    mu = (n * y.bar + a * mu) / (n + a),
    sigma2 = ((n - 1) * s2 + b * sigma2 + 
        a * n * (y.bar - mu)^2 / (n + a)) / (n + b), 
    a = n + a, 
    b = n + b, 
    alpha = b / 2, 
    beta = b * sigma2 / 2
  ))
```

```{R}
#| code-fold: true
#| echo: !expr knitr::is_html_output()
N <- 1e6
bins <- 100

sim.prior.gpt <- tibble(
    sigma2 = rinvgamma(N, 
      prior.gpt$alpha, 
      prior.gpt$beta), 
    mu = rnorm(sigma2, 
      prior.gpt$mu, 
      sqrt(sigma2 / prior.gpt$a)), 
    prec = 1/sigma2,
    model = "prior"
  )

sim.posterior.poison.gpt <- tibble(
    sigma2 = rinvgamma(N, 
        posterior.poison.gpt$alpha, 
        posterior.poison.gpt$beta), 
    mu = rnorm(sigma2, 
        posterior.poison.gpt$mu, 
        sqrt(sigma2 / posterior.poison.gpt$a)), 
    prec = 1/sigma2,
    model = "posterior.poison"
  )
sim.posterior.edible.gpt <- tibble(
    sigma2 = rinvgamma(N, 
        posterior.edible.gpt$alpha, 
        posterior.edible.gpt$beta), 
    mu = rnorm(sigma2, 
        posterior.edible.gpt$mu, 
        sqrt(sigma2 / posterior.edible.gpt$a)), 
    prec = 1/sigma2,
    model = "posterior.edible"
  )

sim.gpt <- rbind(sim.prior.gpt, sim.posterior.poison.gpt, 
                 sim.posterior.edible.gpt)
```

We can now plot the posterior densities and compare it to now a proper prior (recall that Jeffreys prior was *improper*, i.e., it was not a real probability distribution), see @fig-chatgpt-posterior.

```{R}
#| code-fold: true
#| echo: !expr knitr::is_html_output()
#| layout-ncol: 2
#| layout-nrow: 2
#| label: fig-chatgpt-posterior
#| fig-cap: "Simulation of posterior probability with ChatGPT-given prior"
#| fig-subcap: 
#|   - "simulation of the expected value"
#|   - "excepted value kernel estimate"
#|   - "simulation of the variance"
#|   - "variance kernel estimate"

sim.gpt |> ggplot(aes(x = mu, color = model, 
        fill = model, y = after_stat(density))) + 
    geom_histogram(bins = bins, color = "black", size = 0.5) + 
    labs(x = "expected value of stem height", y = "density") + 
    facet_wrap("model", nrow = 3, scales = "free")
sim.gpt |> ggplot(aes(x = mu, color = model, fill = model)) + 
    geom_density(linewidth = 0.7, alpha = 0.1, trim = FALSE) + 
    labs(x = "expected value of stem height", y = "density")

sim.gpt |> ggplot(aes(x = sigma2, color = model, 
        fill = model, y = after_stat(density))) + 
    geom_histogram(bins = bins, color = "black", size = 0.5) + 
    labs(x = "variance of stem height", y = "density") + 
    facet_wrap("model", nrow = 3, scales = "free")
sim.gpt |> ggplot(aes(x = sigma2, color = model, fill = model)) + 
    geom_density(linewidth = 0.7, alpha = 0.1) + 
    labs(x = "variance of stem height", y = "density")
```

Last but not least, we should compare the posterior statistics of Jeffreys and ChatGPT priors, see @tbl-chatgpt-posterior-stats and @tbl-jeffreys-posterior-stats. After close inspection, one can notice that MAP estimate for $\rnd \mu$ has *decreased* by using the ChatGPT prior, which stands to reason as the prior expected lower values in $\rnd \mu$ (see @fig-chatgpt-posterior-2).

```{R}
#| code-fold: true
#| echo: !expr knitr::is_html_output()
#| label: tbl-chatgpt-posterior-stats
#| tbl-cap: Statistics of posterior distributions for $\rnd \mu$ and $\rnd \sigma^2$ using ChatGPT proper prior
sim.gpt |> 
  group_by(model) |> 
  summarise(
    mu.MAP = density.max(mu, 
		  from = 1.5, to = 2.0, n = 1024), 
    mu.mean = mean(mu), 
    mu.sd = sd(mu), 
    mu.ETI.l = t(quantile(mu, c(0.025))),
    mu.ETI.h = t(quantile(mu, c(0.975))),
    mu.HDI.l = t(hdi(mu, credMass = 0.95))[1], 
    mu.HDI.h = t(hdi(mu, credMass = 0.95))[2], 
    
    sigma2.MAP = density.max(sigma2, from = 0.1, 
		  to = 0.25, n = 1024), 
    sigma2.mean = mean(sigma2), 
    sigma2.sd = sd(sigma2), 
    sigma2.ETI.l = t(quantile(sigma2, c(0.025))),
    sigma2.ETI.h = t(quantile(sigma2, c(0.975))),
    sigma2.HDI.l = t(hdi(sigma2, credMass = 0.95))[1], 
    sigma2.HDI.h = t(hdi(sigma2, credMass = 0.95))[2],  
  )
```

