# Zaměnitelnost

:::{.hidden}
{{< include mathematics.qmd >}}
:::

:::{.proof}
Nechť $\rnd \Theta \sim p(\tht)$ a předpokládejme, že
$$
\rY_1, \dots, \rY_n \condp \tht \sim \text{ i.i.d. } f(y_k \condp \tht)
$$
a chceme ukázat, že jsou $\rY_1, \dots, \rY_n$ zaměnitelné. Pak jejich simultální hustota je (podle podmíněné nezávislosti a komutativity součinu)
\begin{align*}
f(y_1, \dots, y_n) &= \int_{\mcal T} \underbrace{f(y_1, \dots, y_n \condp \tht) p(\tht)}_{g(y_1, \dots, y_n, \tht)} \, \d \tht \\
	&= \int_{\mcal T} \prod_{k = 1}^n f(y_k \condp \tht) p(\tht) \, \d \tht \\
	&= \int_{\mcal T} \prod_{k = 1}^n f(y_{i_k} \condp \tht) p(\tht) \, \d \tht \\
	&= \int_{\mcal T} f(y_{i_1}, \dots, y_{i_n} \condp \tht) p(\tht) \, \d \tht \\
	&= f(y_{i_1}, \dots, y_{i_n}).
\end{align*} 
:::

:::{.proof}
Jak jsme ukázali výše, z $\rY_1, \dots, \rY_n \condp \sim \text{ i.i.d. } f(y_k \condp \tht)$ a $\rnd \Theta \sim p(\tht)$ plyne, že $\rY_1, \dots, \rY_n$ jsou zaměnitelné.

Dále podle *de Finettiho* věty, která říká, že
$$
f(y_1, \dots, y_n) = \int_{\mcal T} \prod_{k = 1}^n f(y_k \condp \tht) p(\tht) \, \d \tht,
$$
jsou-li $\rY_1, \dots, \rY_n$ pro $\forall n \in \N$ (tzn. tento směr vyžaduje, aby to fungovalo pro libovolně dlouhé, i nekonečné, posloupnosti). Poznamenejme, že zde neznáme $f, p$, ale známe simultální pravděpodobnost, což je opačná situace nežli výše. Pak $\rY_1, \dots, \rY_n \iid f(y_k \condp \tht)$.
:::

:::{#exm-seniors}
Nechť zkoumáme spokojenost $\tht$ ve skupině 1272 osob. Někdo nám navíc řekl $\tht = 0.10$. Potom
$$
P(\rY_{10} = 1 \condp \tht) = 0.1,
$$
dále
$$
P(\rY_{3} = 1 \condp \rY_1 = y_1, \rY_2 = y_2, \tht) = 0.1
$$
a obdobně bychom mohli pokračovat, či také se můžeme ptát
$$
p(\rY_2 = 1 \condp \rY_1 = y_1, \rY_3 = y_3, \tht) = 0.1
$$
:::

## Hierarchické modely