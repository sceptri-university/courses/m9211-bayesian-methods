let
  pkgs = import <nixpkgs> {};
  unstablePkgs = import <nixos-unstable> {};
  lib = import <lib> {};
  config = import <config> {};
  configPath = builtins.getEnv "NIXOS_CONFIGURATION_DIR" + "/.";
  quartoPreRelease = extraRPackages:
    pkgs.callPackage (configPath + "/packages/quarto/preRelease.nix")
    (with pkgs lib config; {extraRPackages = extraRPackages;});

  rPackages = with pkgs.rPackages; [
    quarto
    tidyverse
    ggplot2
    dplyr
    rmarkdown
    languageserver
    VGAM
    TailRank
    HDInterval
    hdrcde
    invgamma
    GGally
    purrr
  ];
in
  pkgs.mkShell {
    buildInputs = with pkgs; [
      texlive.combined.scheme-full
      pandoc
      xclip
      (
        rWrapper.override {
          packages = rPackages;
        }
      )
      (
        rstudioWrapper.override {
          packages = rPackages;
        }
      )
      (
        quartoPreRelease rPackages
      )
    ];
    shellHook = ''
      echo "Welcome to M9211 Bayesian methods shell"
    '';
  }
