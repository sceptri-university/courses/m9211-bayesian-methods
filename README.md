# M9211 Bayesian methods

This repository follows the course M9211 Bayesian methods taught by Mgr. Ondřej Pokora, Ph.D., on SCI MUNI as it appeared in Spring 2024. This repository is split into *`lectures` & `labs`*, where lie the notes taken during the actual semester, *`project`* which hosts all things project-related, and last but not least *`prep`*, where the preparation for the final exam is located (**this is the most import folder**).

*This course introduces the basic principles and methods of the Bayesian statistics. The student learns the theoretical principles of these methods and the techniques for calculation of the estimators and their application in inference and prediction. The course also deals with the basics of information theory and with the numerical and simulation methods of the calculations. In the practical classes, the student learns how to calculate the aposterior density and the Bayesian estimators in real problems, how to interpret them and how to compare them with the classical statistical estimators. Further, the student goes through the computer implementation of the methods of numerical integration and Markov-Chain-Monte-Carlo simulations.*


## Running

This book is created in Quarto and comes with 2 targets: PDF and HTML. You can render them both in the **`prep`** folder by running
```
quarto render --to all
```

Or similarly in the **`project`** folder -- the rest can be rendered in a similar fashion, but is not of much interest.

## Notice

I typesetted this entire material based on Pokora's presentation and as such I claim only the right to this material itself (which I distribute under the MIT license) and **not** the selection and style of presentation of the content.

## Contribution

Please, feel free to open an Issue or a Merge Request in case something seems wrong.