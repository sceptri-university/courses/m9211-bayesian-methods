# Binomický model

:::{.hidden}
{{< include mathematics.qmd >}}
:::

```{R}
#| code-fold: true
#| output: false
library("tidyr")
library("dplyr")
library("ggplot2")
```

Předpokládejme, že jsme připínáčkem 30 krát hodili, tj, $n = 30$, a 11 krát spadl hrotem nahoru, tj. $y = 11$. Navíc
$$
f(y \condp \tht) = P(\rnd Y = y \condp \tht) = \binom n y \tht^y (1 - \tht)^{n-y}, \quad y = 0, 1, \dots, n, \; \tht \in [0,1]
$$

## Frekventistický přístup

```{R}
n <- 30
df <- expand_grid(
    n = n, 
    y = seq(0, n, by = 1), 
    theta = c(0.05, 0.25, 0.5, 0.75, 0.95)
    )
df1 <- df |> mutate(
    f = dbinom(y, n, theta), # new column
    theta = factor(theta)
    )
df1 |> ggplot(aes(x = y, y = f, colour = theta)) + 
    geom_line(linetype = "dashed", linewidth = 0.2) + 
    geom_point(size = 1.5) + 
    labs(x = "pocet licu", y = "pravdepodobnost")
```

Zde v `ggplot` volání:

- `aes` (*aesthetics*): na ose $x$ bude sloupec z tabulky $y$ apod.

Podívejme se ještě na výstup `expand_grid`, která kombinuje "každý s každým"
```{R}
str(df)
```
a
```{R}
head(data.frame(df), n = 10) # simply for pretty 
```

::: {.callout-tip}
## Pipe operátor

Operátor řetězení (pipe), který funguje cca jako `\circ`:
```{R}
2 |> sum(3) == sum(2,3)
```
:::

Podobně to můžeme vykreslit do více grafů

```{R}
df1 |> ggplot(aes(x = y, y = f, colour = theta)) + 
    geom_line(linetype = "dashed", linewidth = 0.2) + 
    geom_point(size = 1.5) + 
	# or with scales="free_y" it rescales the y axis
    facet_wrap(theta ~ ., ncol = 2) + 
    labs(x = "pocet licu", y = "pravdepodobnost")
```

## Maximální věrohodný odhad

Připomeňme si, že
$$
f(y \condp \tht) = P(\rnd Y = y \condp \tht) = \binom n y \tht^y (1 - \tht)^{n-y}, \quad y = 0, 1, \dots, n, \; \tht \in [0,1]
$$
a věrohodnost je součin hustot pro každé pozorování, tedy v našem případě
$$
\likely(\tht) = f(y \condp \tht) \implies \loglikely(\tht) = \log f(y \condp \tht)
$$
a tedy
$$
\loglikely = \log \binom n y + n \log \tht + (n-y) \log (1 - \tht).
$$

Z tohoto derivováním podle $\tht$ dostáváme
$$
\partialDeriv {l(\tht)} \tht = \frac n \tht - \frac {n-y} {1 - \tht} = 0
$$
a řešením
$$
\begin{gathered}
\frac y \tht = \frac {n-y} {1 - \tht} \\
y(1 - \tht) = \tht (n-y) \\
-y\tht -n\tht + y \tht = -y \\
\estimML \tht = \frac y n.
\end{gathered}
$$ 

::: {.callout-note}
Logaritmus je monotónní funkce, tedy se poloha extrémů nemění.
:::

Věrohodnostní funkci si samozřejmě můžeme i vykreslit a ověřit naše výpočty
```{R}
n <- 30
y <- 11

d.likelihood <- tibble( # modern data.frame
    theta = seq(0, 1, by = 0.001), 
    type = "likelihood"
  ) |> 
  mutate(
      f = dbinom(y, n, theta), #y,n variables, theta is a column
	  lf = log(f)
    )

# tibble prints with types and other useful stuff
head(d.likelihood, n=10)

d.likelihood |> ggplot(aes(x = theta, y = f)) + 
  geom_line(linewidth = 0.8, color = "black") + 
  labs(x = "probability of head", y = "likelihood")

likely.analytic <- y/n
likely.analytic
```

Nicméně my jsme maximalizovali logaritmus této funkce
```{R}
#| warning: false
d.likelihood |> ggplot(aes(x = theta, y = f)) + 
  geom_line(linewidth = 0.8, color = "black") + 
  labs(x = "probability of head", y = "likelihood") + 
  scale_y_log10()
```

::: {.callout-important}
Všimněme si, že logartimus věrohodnosti je **konkávní** a tedy dobře optimalizovatelný.
:::

```{R}
#| warning: false
d.likelihood |> slice(which.max(f))
(w <- optimize(\(theta) dbinom(y, n, theta), interval = c(0, 1), maximum = TRUE))
w
```

To můžeme i zobrazit
```{R}
MLE <- tibble(
  estimate = w$maximum, 
  lower = estimate - qnorm(0.975) * estimate, 
  upper = estimate + qnorm(0.975) * estimate
  )
MLE
```
a
```{R}
d.likelihood |> ggplot(aes(x = theta, y = f)) + 
  geom_line(linewidth = 0.8, color = "black") + 
  geom_vline(xintercept = MLE$estimate, linetype = "longdash", linewidth = 0.7) + 
  labs(x = "probability of head", y = "likelihood")
```

## Bayesovský přístup

Uvažujme nyní aposteriorní hustotu
$$
p(\tht \condp y) = \frac {p(\tht) f(y \condp \tht)} {\int_0^1 p(\tht) f(y \condp \tht)\, \d \tht},
$$
kde $p(\tht \condp y)$ je *aposteriorní pravědpodobnost*, $p(\tht)$ *apriorní* a $f(y \condp \tht)$ *věrohodnost* (likelyhood/sampling distribution), a pro naše čísla
$$
p(\tht \condp y) = \frac {1 \cdot \binom n y \tht^y (1 - \tht)^{n-y}} {\int_0^1 1 \cdot \binom n y \tht^y (1 - \tht)^{n-y} \, \d \tht} = \frac { \tht^y (1 - \tht)^{n-y}} {\int_0^1 \tht^y (1 - \tht)^{n-y} \, \d \tht} = \frac {\tht^y (1 - \tht)^{n-y}}{\betaF(y+1, n-y+1)},
$$
kde Beta funkce je daná
$$
\betaF(a,b) = \int_0^1 \tht^{a-1} (1 - \tht)^{b-1} \, \d \tht.
$$
V našem případě
```{R}
beta(12, 20)
```

Nyní tedy vykreslíme graf hustoty apriorní hustoty
```{R}
d.prior <- tibble(
    theta = seq(0, 1, by = 0.001), 
    type = "prior"
  ) |> mutate(
    p = dunif(theta, 0, 1)
  )

d.prior |> ggplot(aes(x = theta, y = p)) + 
  geom_line(linewidth = 0.8) + 
  labs(x = "probability of head", y = "density")
```
a obdobně i aposteriorního
```{R}
d.posterior <- tibble(
    theta = seq(0, 1, by = 0.001), 
    type = "posterior"
  ) |> mutate(
    p = dbeta(theta, y + 1, n - y + 1)
  )

d.posterior |> ggplot(aes(x = theta, y = p)) + 
  geom_line(linewidth = 0.8) + 
  labs(x = "probability of head", y = "density")
```

To můžeme i srovnat v jednom grafu
```{R}
d.prior |> 
  bind_rows(d.posterior) |> 
  mutate(type = factor(type, 
  	levels = c("posterior", "prior", "likelihood"))
  ) |>
  ggplot(aes(x = theta, y = p, color = type, fill = type)) + 
  geom_line(linewidth = 0.8) + 
  labs(x = "probability of head", y = "density") + 
  scale_color_brewer(palette = "Set1", aesthetics = c("color", "fill"))
```

Navíc to můžeme i srovnat s věrohodností (ve smyslu MLE)
```{R}
d.likelihood |> 
  mutate(
    p = 20 * f
  ) |> 
  bind_rows(d.prior, d.posterior) |> 
  mutate(type = factor(type, levels = c("posterior", "prior", "likelihood"))) |>
  ggplot(aes(x = theta, y = p, color = type, fill = type)) + 
  geom_line(linewidth = 0.8) + 
  labs(x = "probability of head", y = "density") + 
  scale_color_brewer(palette = "Set1", aesthetics = c("color", "fill"))
```

::: {.callout-caution}
Věrohodnost **není** hustota, ale případě tohoto prioru ji můžeme přeškálovat na hustotu.
:::


```{R}
n <- 30
y <- c(0, 1, 6, 11, 15, 19, 30)
theta <- seq(0, 1, by = 0.001)
df <- expand_grid(
    y = y, 
    n = n, 
    theta = theta
    )
df1 <- df |> mutate(
    h = dbeta(theta, 1 + y, 1 + n - y), 
    y = factor(y), 
    n = factor(n)
    )
df1 |> ggplot(aes(x = theta, y = h, colour = y:n)) + 
  geom_line(size = 1) + 
  xlab("theta") + ylab("aposteriorni hustota")
```