---
format:
  html:
    theme:
      light: cerulean
      dark: darkly
    toc: true
    html-math-method: mathjax
    df-print: kable
---

# Model pro teplotu těla

:::{.hidden}
{{< include mathematics.qmd >}}
:::

## Teorie

Předpokládejme, že máme
$$
\rY_K \sim \normalD{\mu, \sigma^2},
$$
přičemž $\mu$ je známe. Jaká je potom Jeffreysova hustota $p(\sigma^2) = ?$. Víme, že
$$
\log f = - \frac n 2 \log (2 \pi \sigma^2) - \frac 1 {2\sigma^2} \sim_{k = 1}^n (y_k - \mu)^2,
$$
potom
\begin{align*}
\partialOp {\sigma^2} \log f &= - \frac n 2 \frac {2\pi} {2 \pi \sigma^2} - \frac 1 2 \brackets{- \frac 1 {\sigma^4}} \sum_{k = 1}^n (y_k - \mu)^2 \\
\frac {\partial ^2} {(\partial \sigma^2)^2} \log f &= \frac n {2 \sigma^4} - \frac 1 {\sigma^6} \sum_{k = 1}^n (y_k - \mu)^2
\end{align*}

Potom 
\begin{align*}
J(\sigma^2) &= -\Expect{ \frac {\partial ^2} {(\partial \sigma^2)^2} \log f(\mu) } \\
  &= - \brackets{\frac n {2 \sigma^4} - \frac 1 {\sigma^6} \sum_{k = 1}^n \underbrace{\Expect{(\rY_k - \mu)^2}}_{\variance \rY_k}} \\
  &= - \frac n {2 \sigma^4} + \frac n {\sigma^4} = \frac n {\sigma^4}
\end{align*}

Celkem pak $p(\sigma^2) \propto \sqrt{J(\sigma^2)}$ a tedy
$$
p(\sigma^2) \propto \frac 1 {\sigma^2}
$$

## Praxe


```{R}
#| code-fold: true
#| output: false
library("HDInterval")
library("invgamma")
library("tibble")
library("tidyr")
library("dplyr")
library("ggplot2")
```

```{R}
data.base <- read.csv("data/bodytemps.csv")
head(data.base)
```

```{R}
data <- data.base |> mutate(
  temp = 5/9 * (tempF - 32)
)
```

```{R}
data |> ggplot(aes(y = sex, x = temp, fill = sex)) + 
  geom_boxplot(color = "black", alpha = 0.3, outlier.shape = NA) + 
  geom_jitter(pch = 21, size = 2.5, width = 0, height = 0.1) + 
  labs(x = "teplota tela", y = "pohlavi")
```

```{R}
# Hodnoty statistik z experimentu 
y.bar.m <- data |> filter(sex == "male") |> pull(temp) |> mean()
s2.m <- data |> filter(sex == "male") |> pull(temp) |> var()
n.m <- data |> filter(sex == "male") |> pull(temp) |> length()
y.bar.f <- data |> filter(sex == "female") |> pull(temp) |> mean()
s2.f <- data |> filter(sex == "female") |> pull(temp) |> var()
n.f <- data |> filter(sex == "female") |> pull(temp) |> length()

# Parametry apriorniho rozdeleni
# beta / (alpha - 1) => b * sigma2 / (b-2)
# prior <- tibble(b = 3, sigma2 = 2/30, mu = 37, a = 2, alpha = b / 2, beta = b * sigma2 / 2)
prior <- tibble(b = 5, sigma2 = 0.12, mu = 37, a = 5, alpha = b / 2, beta = b * sigma2 / 2)
# Parametry aposteriornich rozdeleni
posterior.m <- with(prior, tibble(
    y.bar = y.bar.m,
    s2 = s2.m,
    n = n.m,
    mu = (n * y.bar + a * mu) / (n + a),
    sigma2 = ((n - 1) * s2 + b * sigma2 + a * n * (y.bar - mu)^2 / (n + a)) / (n + b), 
    a = n + a, 
    b = n + b, 
    alpha = b / 2, 
    beta = b * sigma2 / 2
  ))
posterior.f <- with(prior, tibble(
    y.bar = y.bar.f,
    s2 = s2.f,
    n = n.f,
    mu = (n * y.bar + a * mu) / (n + a),
    sigma2 = ((n - 1) * s2 + b * sigma2 + a * n * (y.bar - mu)^2 / (n + a)) / (n + b), 
    a = n + a, 
    b = n + b, 
    alpha = b / 2, 
    beta = b * sigma2 / 2
  ))
```

```{R}
prior
```

```{R}
posterior.m
```

```{R}
posterior.f
```

```{R}
# generovani vzorku inverzniho gamma rozdeleni a nasledne normalniho rozdeleni
N <- 1e5
bins <- 100
sim.prior <- tibble(
    sigma2 = rinvgamma(N, prior$alpha, prior$beta), 
    mu = rnorm(sigma2, prior$mu, sqrt(sigma2 / prior$a)), 
    prec = 1/sigma2,
    model = "prior"
  )
sim.posterior.m <- tibble(
    sigma2 = rinvgamma(N, posterior.m$alpha, posterior.m$beta), 
    mu = rnorm(sigma2, posterior.m$mu, sqrt(sigma2 / posterior.m$a)), 
    prec = 1/sigma2,
    model = "posterior.m"
  )
sim.posterior.f <- tibble(
    sigma2 = rinvgamma(N, posterior.f$alpha, posterior.f$beta), 
    mu = rnorm(sigma2, posterior.f$mu, sqrt(sigma2 / posterior.f$a)), 
    prec = 1/sigma2,
    model = "posterior.f"
  )
sim <- rbind(sim.prior, sim.posterior.m, sim.posterior.f)
```


```{R}
#| layout-ncol: 2
#| fig-cap: 
#|   - "Simulace"
#|   - "Jádrový odhad"

# marginalni rozdeleni
sim |> ggplot(aes(x = mu, color = model, fill = model, y = after_stat(density))) + 
    geom_histogram(bins = bins, color = "black", size = 0.5) + 
    labs(x = "stredni hodnota teploty tela", y = "hustota") + 
    xlim(c(36, 38)) +
    facet_wrap("model", nrow = 3, scales = "free_y")
sim |> ggplot(aes(x = mu, color = model, fill = model)) + 
    geom_density(linewidth = 0.7, alpha = 0.1) + 
    labs(x = "stredni hodnota teploty tela", y = "hustota") + 
    xlim(c(36, 38))
```

```{R}
#| layout-ncol: 2
#| fig-cap: 
#|   - "Simulace"
#|   - "Jádrový odhad"

sim |> ggplot(aes(x = sigma2, color = model, fill = model, y = after_stat(density))) + 
    geom_histogram(bins = bins, color = "black", size = 0.5) + 
    labs(x = "rozptyl teploty tela", y = "hustota") + 
    xlim(c(0, 0.5)) +
    facet_wrap("model", nrow = 3, scales = "free_y")

sim |> ggplot(aes(x = sigma2, color = model, fill = model)) + 
    geom_density(linewidth = 0.7, alpha = 0.1) + 
    labs(x = "rozptyl teploty tela", y = "hustota") + 
    xlim(c(0, 0.5))
```

```{R}
#| layout-ncol: 2
#| fig-cap: 
#|   - "Simulace"
#|   - "Jádrový odhad"

sim |> ggplot(aes(x = prec, color = model, fill = model, y = after_stat(density))) + 
    geom_histogram(bins = bins, color = "black", size = 0.5) + 
    labs(x = "rozptyl teploty tela", y = "hustota") + 
    xlim(c(0, 20)) +
    facet_wrap("model", nrow = 3, scales = "free_y")

sim |> ggplot(aes(x = prec, color = model, fill = model)) + 
    geom_density(linewidth = 0.7, alpha = 0.1) + 
    labs(x = "rozptyl teploty tela", y = "hustota") + 
    xlim(c(0, 20))
```


```{R}
#| layout-ncol: 2
#| fig-cap: 
#|   - "Rozptyl"
#|   - "Přesnost"
# simultanni rozdeleni
sim |> ggplot(aes(x = mu, y = sigma2, group = model, color = model)) + 
  geom_point(size = 0.5, alpha = 0.5) +
    geom_density_2d(color = "black", linewidth = 0.5, bins = 15) + 
    labs(x = "stredni hodnota teploty tela", y = "rozptyl teploty tela") + 
    facet_wrap("model", ncol = 3, scales = "fixed") + 
  ylim(c(0, 1)) + 
  xlim(c(36, 38))

# simultanni rozdeleni
sim |> ggplot(aes(x = mu, y = prec, group = model, color = model)) + 
  geom_point(size = 0.5, alpha = 0.5) +
    geom_density_2d(color = "black", linewidth = 0.5, bins = 15) + 
    labs(x = "stredni hodnota teploty tela", y = "presnost teploty tela") + 
    facet_wrap("model", ncol = 3, scales = "fixed") + 
  ylim(c(0, 20)) + 
  xlim(c(36, 38))
```

```{R}
# Ne prilis dokonala funkce pro numericke hledani maxima jadroveho odhadu hustoty
density.max <- function(x, ...) {
  pdf.kde <- density(x, ...)
  index <- which.max(pdf.kde$y)
  return(pdf.kde$x[index])
}

sim |> 
  group_by(model) |> 
  summarise(
    mu.MAP = density.max(mu, from = 1.5, to = 2.5, n = 1024), 
    mu.mean = mean(mu), 
    mu.sd = sd(mu), 
    mu.ETI = t(quantile(mu, c(0.025, 0.975))),
    mu.HDI = t(hdi(mu, credMass = 0.95)), 
    sigma2.MAP = density.max(sigma2, from = 0, to = 0.15, n = 1024), 
    sigma2.mean = mean(sigma2), 
    sigma2.sd = sd(sigma2), 
    sigma2.ETI = t(quantile(sigma2, c(0.025, 0.975))),
    sigma2.HDI = t(hdi(sigma2, credMass = 0.95)), 
  )
head(sim)
```

```{R}
# Prediktivni rozdeleni je normalni se simulovanymi parametry mu a sigma2
# generovani vzorku z aprirorniho a aposteriornich prediktivnich rozdeleni
sim.pred <- sim |> 
  mutate(
    z = rnorm(mu, mu, sqrt(sigma2)), 
    model = ifelse(model == "prior", "marginal", model)
  )
head(sim.pred)
```

```{R}
#| layout-ncol: 2
#| fig-cap: 
#|   - "Simulace"
#|   - "Jádrový odhad"

sim.pred |> ggplot(aes(x = z, color = model, fill = model, y = after_stat(density))) + 
    geom_histogram(bins = bins, color = "black", size = 0.5) + 
    labs(x = "predikovana teplota tela", y = "hustota") + 
  xlim(c(36, 38)) +
    facet_wrap("model", nrow = 3, scales = "free_y")

sim.pred |> ggplot(aes(x = z, color = model, fill = model)) + 
    geom_density(linewidth = 0.7, alpha = 0.1) + 
    labs(x = "predikovana teplota tela", y = "hustota") + 
  xlim(c(36, 38))
```