:::{.content-visible when-format="html"}
```{=html}
$$
{{< include ../mathematics/html-math.tex >}}
$$
$$
{{< include ../mathematics/general.tex >}}
$$
$$
{{< include ../mathematics/linear-algebra.tex >}}
$$
$$
{{< include ../mathematics/statistics.tex >}}
$$
$$
{{< include ../mathematics/special.tex >}}
$$
```
:::