using Plots

ε = 1e-5

# 5)
H_A(θ) = -((1 - θ) * log2(1 - θ) + θ * log2(θ))
θ_space = ε:0.01:(1 - ε)
plot(θ_space, H_A)
θ_space[argmax(H_A.(θ_space))]

